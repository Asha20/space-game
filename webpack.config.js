const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const htmlMinifyOptions = {
  collapseWhitespace: true,
  removeComments: true,
  removeRedundantAttributes: true,
  removeScriptTypeAttributes: true,
  removeStyleLinkTypeAttributes: true,
  useShortDoctype: true,
};

const NODE_ENV = process.env.NODE_ENV;
const isProduction = NODE_ENV === "production";

module.exports = {
  mode: isProduction ? "production" : "development",
  entry: "./src/ts/main.ts",
  devtool: "source-map",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "main.js",
  },
  devServer: {
    clientLogLevel: "silent",
    port: 8000,
    hot: true,
  },
  resolve: {
    extensions: [".js", ".ts", ".json"],
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: "awesome-typescript-loader",
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: "src/templates/game.html",
      minify: isProduction && htmlMinifyOptions,
    }),
  ],
};
